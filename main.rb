require 'time'
require 'yaml'
def day_and_month(date)
  date[5..-1]
end
def find_days_after_today(calendar, today)
  if (calendar.keys.select { |date| day_and_month(date) >= today }).empty?
    return calendar.keys.min
  else
    return calendar.keys.select { |date| day_and_month(date) >= today }
  end
end
def make_choise
  choises = {
    "1" => :enter_event,
    "2" => :show_database,
    "3" => :show_next
  }
  choises.default = :quit
  print <<~TXT
    Press key:
      1 - to enter the event into the database
      2 - to show database
      3 - to show next event
      other key to exit
  TXT
choises[gets.chomp]
end
def enter_the_date
  puts 'Enter the date'
  date = Date.parse(gets.chomp())
  File.open('lol.txt', 'a'){ |file| file.puts date.to_yaml }
  puts 'Enter the event'
  event = gets.chomp()
  File.open('lol.txt', 'a'){ |file| file.puts event.to_yaml }
  puts 'Changes applied!'
end
def show_database
  temp = File.open('lol.txt', 'r'){ |file| file.read }
  puts 'Your Data Base:'
  puts temp
end
def show_next_event
  today = Time.new.strftime("%m-%d")
  puts "Today: #{today}"
  temp = File.open('lol.txt', 'r'){ |file| file.read }.split(/\n/)
  calendar = Hash[*temp]
  puts "Next date: "
  puts find_days_after_today(calendar, today).map {|date| day_and_month(date)}.min
end
case make_choise
when :enter_event
  enter_the_date
when :show_database
  show_database
when :show_next
  show_next_event
end
